#!/usr/bin/env bash

echo "Setting vhosts"

sudo echo "127.0.0.1 	local.shopping.com" >> /etc/hosts

sudo cp -f /vagrant/conf/vhost/shopping.conf /etc/apache2/sites-available/shopping.conf

sudo a2ensite shopping.conf 

sudo service apache2 restart